package ru.terrakok.gitlabclient.toothpick

/**
 * @author Konstantin Tskhovrebov (aka terrakok) on 09.07.17.
 */
object DI {
    const val APP_SCOPE = "app scope"
    const val SERVER_SCOPE = "server scope"

    const val MAIN_ACTIVITY_SCOPE = "main activity scope"
    const val USER_SCOPE = "user scope"
    const val PROJECT_SCOPE = "project scope"
    const val MERGE_REQUEST_SCOPE = "merge request scope"
    const val ISSUE_SCOPE = "issue scope"
}